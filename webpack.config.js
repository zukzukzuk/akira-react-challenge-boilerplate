var path = require('path')
var webpack = require('webpack')

var devHost = "localhost"
var devPort = 3000
var akiraRestUrl = "http://challenge-api.akira.md/api"
var buildDir = 'build'
var publicBuildDir = '/'+buildDir+'/'
var enableHotReload = true
var devtool = 'cheap-module-eval-source-map'

if (process.env.AKIRA_REST_URL) {
  akiraRestUrl = process.env.AKIRA_REST_URL
}


var plugins = [
  new webpack.DefinePlugin({
    'process.env': {
      // Useful to reduce the size of client-side libraries, e.g. react
      NODE_ENV: JSON.stringify(process.env.NODE_ENV)
    },
    AKIRA_REST_URL: JSON.stringify(akiraRestUrl)
  })
]

var entry = [
  './index.jsx'
]

if (enableHotReload) {
  entry = [].concat('webpack-hot-middleware/client')
            .concat(entry)

  plugins = plugins.concat(new webpack.HotModuleReplacementPlugin())
}


plugins = plugins.concat(new webpack.NoErrorsPlugin())

module.exports = {
  devtool: devtool,
  entry: entry,
  output: {
    path: path.join(__dirname, buildDir),
    filename: 'bundle.js',
    publicPath: publicBuildDir
  },
  resolve: {
    extensions: ['', '.jsx', '.js', '.scss', '.json']
  },
  plugins: plugins,
  module: {
    loaders: [
      {
        test: /(\.js|\.jsx)$/,
        loader: 'babel',
        query: {
          presets: ['es2015', 'react']
        },
        exclude: /node_modules/,
        include: __dirname
      },
      {
        test: /\.scss$/,
        loaders: ['style', 'css', 'sass']
      },
      {
        test: /\.(png|jpg|svg|WOFF)$/,
        loader: 'url',
        query: {
          limit: 256
        }
      }
    ]
  }
}


// When inside Redux repo, prefer src to compiled version.
// You can safely delete these lines in your project.
var reduxSrc = path.join(__dirname, '..', '..', 'src')
var reduxNodeModules = path.join(__dirname, '..', '..', 'node_modules')
var fs = require('fs')
if (fs.existsSync(reduxSrc) && fs.existsSync(reduxNodeModules)) {
  // Resolve Redux to source
  module.exports.resolve = { alias: { 'redux': reduxSrc } }
  // Compile Redux from source
  module.exports.module.loaders.push({
    test: /(\.js|\.jsx)$/,
    loaders: ['babel'],
    include: reduxSrc
  })
}
