import lodash from 'lodash'
import { LOGIN, RECEIVE_AUTH } from '../actions'

export default function auth(state = {}, action) {
  const authenticated = action.payload && !!action.payload.user

  switch (action.type) {
    case LOGIN:
      return handleLogin(state, action);
    case RECEIVE_AUTH:
      return handleReceiveAuth(state, action)
    default:
      return state
  }
}

function handleLogin(state, action) {
  // IMPLEMENT ME
  return {}
}

function handleReceiveAuth(state, action) {
  return lodash.merge({}, state, {
    authToken: action.payload.token,
    user: action.payload.user,
    authenticated
  })
}
