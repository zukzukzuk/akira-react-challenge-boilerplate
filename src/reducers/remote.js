import lodash from 'lodash'
import { RECEIVE_APPOINTMENTS, RECEIVE_CREATED_APPOINTMENT,
         RECEIVE_AVAILABLE_APPOINTMENT_TIMESLOTS } from '../actions'

const defaultState = {
  appointments: [],
  timeslots: []
}

export default function(state = defaultState, action) {
  switch (action.type) {
    case RECEIVE_APPOINTMENTS:
      return handleReceiveAppointments(state, action)
    case RECEIVE_CREATED_APPOINTMENT:
      return handleReceiveCreatedAppointment(state, action)
    case RECEIVE_AVAILABLE_APPOINTMENT_TIMESLOTS:
      return handleReceiveAvailableAppointmentTimeslots(state, action)
    default:
      return state
  }
}


function handleReceiveAppointments(state, action) {
  return lodash.merge({}, state, {appointments: action.payload})
}

function handleReceiveCreatedAppointment(state, action) {
  return lodash.merge({}, state, {appointments: state.remote.appointments.concat(action.payload)})
}

function handleReceiveAvailableAppointmentTimeslots(state, action) {
  return lodash.merge({}, state, {appointments: state.remote.appointments.concat(action.payload)})
}
