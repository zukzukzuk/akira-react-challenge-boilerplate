import { combineReducers } from 'redux'

import app from './app'
import auth from './auth'
import remote from './remote'

const rootReducer = combineReducers({
  app, auth, remote
})

export default rootReducer
