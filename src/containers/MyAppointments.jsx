import React from 'react'
import lodash from 'lodash'
import moment from 'moment'
import { connect } from 'react-redux'
import { fetchAppointments } from '../actions'

import './MyAppointments.scss'


class MyAppointments extends React.Component {

  constructor() {
    super()
    this.state = {}
  }

  componentWillMount() {
    const { dispatch } = this.props
    dispatch(fetchAppointments())
  }

  render() {
    const { dispatch, appointments } = this.props

    return (
      <div className="MyAppointments">

      </div>
    )
  }
}

MyAppointments.propTypes = {
  dispatch: React.PropTypes.func.isRequired
}

function select(state) {
  return {
    appointments: state.remote.appointments
    // ... state selected for this container
  }
}

export default connect(select)(MyAppointments)
