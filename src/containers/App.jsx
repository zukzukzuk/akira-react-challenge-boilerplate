import React from 'react'
import { connect } from 'react-redux'
import LoginModal from '../components/LoginModal'
import MyAppointments from './MyAppointments'
import { login } from '../actions'

import './App.scss'


class App extends React.Component {

  render() {
    const { dispatch, auth } = this.props

    if (auth.authenticated) {
      var content = <MyAppointments />
    } else {
      var content = <LoginModal
                      auth={auth}
                      login={creds => dispatch(login(creds))}
                      />
    }

    return (
      <div className="App">
        {content}
      </div>
    )
  }
}

App.propTypes = {
  auth: React.PropTypes.object.isRequired
}

function select(state) {
  const { auth } = state
  return {
    auth
  }
}

export default connect(select)(App)
